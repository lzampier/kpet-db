KPET-DB
=======

Kpet-db is a collection of YAML files and Jinja2 templates describing the
tests CKI project runs on Linux kernels, conditions for running them, where to
run them, and how to generate an XML file describing a run, to be supplied to
a [Beaker](https://beaker-project.org/) service for execution.

It is to be used with [kpet](https://gitlab.com/cki-project/kpet) to generate
the XML and query various objects. Execute kpet in the top directory, or
supply the top directory using its `--db` option. Like this:

```console
~$ cd kpet-db
~/kpet-db$ kpet arch list
aarch64
i386
ppc64
ppc64le
s390x
x86_64
```

Or this:
```console
~$ kpet --db ~/kpet-db arch list
aarch64
i386
ppc64
ppc64le
s390x
x86_64
```

Documentation
-------------
See the description of the database's structure in the comments of the
top-level [`index.yaml`](index.yaml).
See the kpet manual in
[its README.md](https://gitlab.com/cki-project/kpet/-/blob/main/README.md).
See the [Beaker User
Guide](https://beaker-project.org/docs/user-guide/index.html) for an
introduction to XML kpet generates, and see the [RELAX NG
schema](https://beaker-project.org/docs/_downloads/beaker-job.rng) describing
it in detail.

### Adding driver-targeted testing

Here's how you can request execution of a particular test with particular
peripheral, when the driver for that peripheral changes. We'll be using
the Ethernet sanity test running with the "bnx2x" driver as an example.

#### Create the host type

Check whether Beaker has any hardware handled by the driver. E.g. use the
Beaker WebUI, or lookup hardware loading the driver's module using this
command:

```bash
bkr list-systems --xml-filter="<key_value key='MODULE' op='=' value='<MODULE>'/>"
```

For "bnx2x" it could look like this:
```console
~$ bkr list-systems --xml-filter="<key_value key='MODULE' op='=' value='bnx2x'/>"
amd-pike-08.klab.eng.bos.redhat.com
ansible-fx2-1.tripleo.lab.eng.rdu2.redhat.com
ansible-fx2-2.tripleo.lab.eng.rdu2.redhat.com
ansible-fx2-3.tripleo.lab.eng.rdu2.redhat.com
...
```

Add another host type under `host_types` in [`index.yaml`](index.yaml). Name
it e.g. `driver_<DRIVER>`, put it into the `shared` domain, and specify the
location of its `host_requires` file, e.g.
`host_types/driver_<DRIVER>.host_requires.xml.j2`

The `host_requires` file should contain any conditions you would put into a
Beaker job XML to select the hosts with the driver's hardware. Following the
commands above it could contain something like this:

```xml
<key_value key='MODULE' op='=' value='<MODULE>'/>
```

So, for "bnx2x" driver we can put this:

```xml
<key_value key='MODULE' op='=' value='bnx2x'/>
```

into the `host_types/driver_bnx2x.host_requires.xml.j2` file, and add a host
type like this:

```yaml
  driver_bnx2x:
    domains: shared
    host_requires: host_types/driver_bnx2x.host_requires.xml.j2
```

#### Write source file regexes

Write regular expressions fully matching paths to driver's source files.
Use the `find` command to test the regular expressions. Like this:

```bash
find * -regex '<REGULAR_EXPRESSION>'
```

Assuming the source files for the "bnx2x" driver are located in the
`drivers/net/ethernet/broadcom/bnx2x` directory, the regular expression would
be `drivers/net/ethernet/broadcom/bnx2x/.*`, and we can test it like this:

```console
~/linux$ find * -regex 'drivers/net/ethernet/broadcom/bnx2x/.*'
drivers/net/ethernet/broadcom/bnx2x/bnx2x.h
drivers/net/ethernet/broadcom/bnx2x/bnx2x_link.h
drivers/net/ethernet/broadcom/bnx2x/bnx2x_mfw_req.h
...
```

#### Fork the test

Find the YAML file containing the description of your test. For example, by
recursive grepping for (a part of) the name you see in CKI project reports.

For the "Ethernet drivers sanity" test it could look like this:

```console
~/kpet-db$ grep -r --include '*.yaml' 'Ethernet drivers sanity' .
./cases/networking/ethernet/drivers/sanity/index.yaml:name: 'Ethernet drivers sanity'
```

Split the test into two cases: one targeting your driver, and another - the
rest of the requirements. To do that, add a `cases` dictionary with two
cases underneath it: one rejecting your source files, and another rejecting
builds without patches and matching your source files. Use the regular
expressions you wrote. Use e.g. the word "general", and the driver's name as
IDs for the cases, respectively.

Specify the host type you created in `host_types` for the case matching
your source files. Set the `name` of your case to the name of your driver.

For example, if originally the `Ethernet drivers sanity` test description
looked like this:

```yaml
name: 'Ethernet drivers sanity'
universal_id: redhat_ethernet
origin: suites_zip
location: networking/driver/sanity
maintainers:
  - name: John Linville
    email: linville@redhat.com
    gitlab: linville-rh
enabled:
  sources:
    or:
      - drivers/net/ethernet/.*
      - net/socket\.c
      - net/unix/.*
      - net/netlink/af_netlink\.h
      - arch/x86/include/asm/fpu/.*
      - arch/x86/entry/common\.c
      - arch/x86/include/asm/e820/api\.h
      - include/net/netns/.*
      - include/trace/syscall\.h
      - include/linux/unaligned/access_ok\.h
      - include/linux/sched/.*
      - security/selinux/include/.*
sets:
  - cloud
  - net
  - kt1
max_duration_seconds: 720
```

then, to target the "bnx2x" driver changes to the host type you created (and
the driver's hardware), change it like this:

```patch
diff --git a/cases/networking/ethernet/drivers/sanity/index.yaml b/cases/networking/ethernet/drivers/sanity/index.yaml
index 5dc87008..aa0a1fba 100644
--- a/cases/networking/ethernet/drivers/sanity/index.yaml
+++ b/cases/networking/ethernet/drivers/sanity/index.yaml
@@ -27,3 +27,17 @@ sets:
   - kt1
 max_duration_seconds: 720
+cases:
+  general:
+    enabled:
+      sources:
+        not:
+          or:
+            - drivers/net/ethernet/broadcom/bnx2x/.*
+  bnx2x:
+    name: bnx2x
+    enabled:
+      sources:
+        - not: null
+        - drivers/net/ethernet/broadcom/bnx2x/.*
+    host_types: driver_bnx2x
```

This driver-targeted test would then appear in reports as "Ethernet drivers
sanity - bnx2x", and the general test would remain named "Ethernet drivers
sanity".

If the test you need was already forked for another driver, exclude your
sources from the general fork, and add another case for your driver.

For example, if you need to target the "mlx5" driver, modify the file like
this (after adding the `driver_mlx5` host type):

```patch
diff --git a/cases/networking/ethernet/drivers/sanity/index.yaml b/cases/networking/ethernet/drivers/sanity/index.yaml
index aa0a1fba..8202800d 100644
--- a/cases/networking/ethernet/drivers/sanity/index.yaml
+++ b/cases/networking/ethernet/drivers/sanity/index.yaml
@@ -30,14 +30,22 @@ max_duration_seconds: 720
 cases:
   general:
     enabled:
       sources:
         not:
           or:
             - drivers/net/ethernet/broadcom/bnx2x/.*
+            - drivers/net/ethernet/mellanox/mlx5/.*
   bnx2x:
     name: bnx2x
     enabled:
       sources:
         - not: null
         - drivers/net/ethernet/broadcom/bnx2x/.*
     host_types: driver_bnx2x
+  mlx5:
+    name: mlx5
+    enabled:
+      sources:
+        - not: null
+        - drivers/net/ethernet/mellanox/mlx5/.*
+    host_types: driver_mlx5
```

The new test would appear in reports as "Ethernet drivers sanity - mlx5", and
the names of the other tests would remain unchanged.
