name: 'xfstests'
universal_id: xfstests
origin: suites_zip
location: filesystems/xfs/xfstests
host_types: xfstests
enabled:
  not:
    or:
      - trees: rhel6.*|eln
      # Disabled on s390x
      # not all s390x have enough disk space to run the test
      # ppc64le bare metal only exists for rhel-8 and that was a tiny group of
      # customers. Everything ppc64le should be lpars
      # LPARS have IO performance issue:
      # https://gitlab.com/redhat/centos-stream/tests/kernel/kernel-tests/-/issues/1119
      - arches: s390x|ppc64le
  components:
    # the test should run for related MRs and on mainline kernel that uses kt1
    # official rhel builds shouldn't run it
    - not: officialbuild
environment:
  # publish subtest results also for sub tests that pass
  TEST_PARAM_REPORT_PASS: "1"
cases:
  default:
    maintainers:
      - name: Xiong Zhou
        email: xzhou@redhat.com
        gitlab: jencce2002
    enabled:
      not:
        trees: autosd|rhivos
    partitions: cases/filesystems/xfs/xfstests/partitions.xml.j2
    sets:
      - fs
      - kt1
    environment:
      TEST_PARAM_TEST_DIR: /mnt/xfstests/mnt1
      TEST_PARAM_SCRATCH_MNT: /mnt/xfstests/mnt2
    trigger_sources:
      - block/.*
      # xfstests have been useful to find issues on scheduler
      - kernel/sched/.*
    cases:
      # EXT4
      ext4:
        name: 'ext4'
        max_duration_seconds: 28800
        target_sources:
          - fs/ext4/.*
          - fs/jbd2/.*
        environment:
          TEST_PARAM_FSTYPE: ext4
          TEST_PARAM_DEV_TYPE: mount
        cases:
          rhel7:
            name: 'rhel7'
            enabled:
              trees: rhel7.*
          non-rhel7:
            enabled:
              not:
                trees: rhel7.*
            environment:
              TEST_PARAM_LOGWRITES_MNT: /mnt/xfstests/logwrites

      # XFS
      xfs:
        name: 'xfs'
        max_duration_seconds: 23400
        target_sources:
          - fs/xfs/.*
        trigger_sources:
          - fs/[^/]*[ch]
        environment:
          TEST_PARAM_FSTYPE: xfs
          TEST_PARAM_DEV_TYPE: mount
        cases:
          rhel7:
            name: 'rhel7'
            enabled:
              trees: rhel7.*
            environment:
              TEST_PARAM_MKFS_OPTS: -m crc=1,finobt=1
          non-rhel7:
            enabled:
              not:
                trees: rhel7.*
            environment:
              TEST_PARAM_LOGWRITES_MNT: /mnt/xfstests/logwrites
              TEST_PARAM_MKFS_OPTS: >-
                -m crc=1,finobt=1,rmapbt=1,reflink=1 -i sparse=1

      # BTRFS
      btrfs:
        name: 'btrfs'
        max_duration_seconds: 23400
        enabled:
          arches:
            or:
              - x86_64
              - aarch64
              - ppc64le
              - s390x
          trees:
            or:
              - fedora-latest|rawhide|eln
        sets:
          - fs
          - kt1
        target_sources:
          - fs/btrfs/.*
        environment:
          TEST_PARAM_DEV_TYPE: mount
          TEST_PARAM_FSTYPE: btrfs

      # NFS
      nfs:
        name: 'nfsv4.2'
        max_duration_seconds: 23400
        enabled:
          arches:
            or:
              - x86_64
          trees:
            or:
              - fedora-latest|rawhide|eln
              - rhel9.*|c9s.*
              - rhel8.*
        sets:
          - fs
          - kt1
        target_sources:
          - fs/nfs/.*
          - fs/nfsd/.*
          - fs/fscache/.*
          - fs/cachefiles/.*
          - net/sunrpc/.*
        environment:
          TEST_PARAM_FSTYPE: nfs4
          TEST_PARAM_DEV_TYPE: nfs
          TEST_PARAM_TEST_FS_MOUNT_OPTS: -o vers=4.2
          TEST_PARAM_MOUNT_OPTS: -o vers=4.2

      # cifs
      cifs:
        name: 'cifsv3.11'
        max_duration_seconds: 23400
        enabled:
          arches:
            or:
              - x86_64
          trees:
            or:
              - fedora-latest|rawhide|eln
              - rhel9.*|c9s.*
              - rhel8.*
        sets:
          - fs
          - kt1
        target_sources:
          - fs/cifs/.*
        trigger_sources:
          - drivers/net/.*
          - net/.*
        environment:
          TEST_PARAM_FSTYPE: cifs
          TEST_PARAM_DEV_TYPE: cifs
          TEST_PARAM_TEST_FS_MOUNT_OPTS: >-
            -o vers=3.11,mfsymlinks,username=root,password=redhat
          TEST_PARAM_MOUNT_OPTS: >-
            -o vers=3.11,mfsymlinks,username=root,password=redhat
  gfs2:
    name: 'gfs2'
    host_types: xfstests_gfs2
    maintainers:
      - name: Andrew Price
        email: anprice@redhat.com
        gitlab: andyprice
    enabled:
      trees:
        or:
          - fedora-latest|rawhide|eln
          - rhel9.*|c9s
          - rhel8.*
      components:
        - not: rt
      not:
        or:
          # gfs2 is not supported on aarch64
          arches:
            or:
              - aarch64

    target_sources:
      - fs/gfs2/.*
    sets:
      - gfs2
    environment:
      TEST_PARAM_TEST_DIR: /mnt/xfstests/mnt1
      TEST_PARAM_SCRATCH_MNT: /mnt/xfstests/mnt2
      TEST_PARAM_FSTYPE: gfs2
      TEST_PARAM_DEV_TYPE: mount
    max_duration_seconds: 28800
  automotive:
    name: 'ext4 - AUTOSD - RHIVOS'
    maintainers:
      - name: Xiong Zhou
        email: xzhou@redhat.com
        gitlab: jencce2002
    enabled:
      trees:
        - autosd|rhivos
    target_sources:
      - fs/ext4/.*
    host_types: normal
    sets:
      - automotive
    environment:
      TEST_PARAM_FSTYPE: ext4
      TEST_PARAM_SKIPTESTS: generic/503
    max_duration_seconds: 28800
